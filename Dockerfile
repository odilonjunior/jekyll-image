FROM alpine

WORKDIR /srv/jekyll
COPY Gemfile /srv/jekyll
RUN apk add --no-cache ruby \
    && apk add --no-cache ruby ruby-dev ruby-bundler ruby-irb ruby-rdoc build-base libffi-dev ruby-webrick \
    && gem update --system \
    && gem install bundler --no-document \
    && gem install bigdecimal --no-document \
    && bundle install \
    && gem cleanup